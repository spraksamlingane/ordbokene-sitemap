# ordbokene-sitemap
Sitemap and sitemap_index for ordbokene.no

Scheduled cicd job that runs sitemap.py
Edit or add static pages, locales and baseurls in config.json

I have only included alternate links to localized versions for the static pages, as the alternate links also are available in the head section of each page.